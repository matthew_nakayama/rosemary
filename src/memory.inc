HardwareType .equ $8000
lastKey .equ HardwareType + 1
KeyBuffer .equ lastKey + 1
StringMemory .equ KeyBuffer + 1
KeyInterruptCounter .equ StringMemory + 1

IntRoutine .equ $8080
IntVector .equ $8100

InputBufferLength .equ StatVars
InputBuffer .equ InputBufferLength + 2